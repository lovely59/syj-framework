package com.syj.boot.adminserver.modules.system.convert.notice;

import com.syj.boot.adminserver.modules.system.dal.dataobject.notice.SysNoticeDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticeCreateReqVO;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticeRespVO;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticeUpdateReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SysNoticeConvert {

    SysNoticeConvert INSTANCE = Mappers.getMapper(SysNoticeConvert.class);

    PageResult<SysNoticeRespVO> convertPage(PageResult<SysNoticeDO> page);

    SysNoticeRespVO convert(SysNoticeDO bean);

    SysNoticeDO convert(SysNoticeUpdateReqVO bean);

    SysNoticeDO convert(SysNoticeCreateReqVO bean);

}
