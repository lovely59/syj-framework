package com.syj.boot.adminserver.modules.infra.convert.config;

import com.syj.boot.adminserver.modules.infra.controller.config.vo.InfConfigCreateReqVO;
import com.syj.boot.adminserver.modules.infra.controller.config.vo.InfConfigExcelVO;
import com.syj.boot.adminserver.modules.infra.controller.config.vo.InfConfigRespVO;
import com.syj.boot.adminserver.modules.infra.controller.config.vo.InfConfigUpdateReqVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.config.InfConfigDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface InfConfigConvert {

    InfConfigConvert INSTANCE = Mappers.getMapper(InfConfigConvert.class);

    PageResult<InfConfigRespVO> convertPage(PageResult<InfConfigDO> page);

    InfConfigRespVO convert(InfConfigDO bean);

    InfConfigDO convert(InfConfigCreateReqVO bean);

    InfConfigDO convert(InfConfigUpdateReqVO bean);

    List<InfConfigExcelVO> convertList(List<InfConfigDO> list);

}
