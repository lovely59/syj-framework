/**
 * 错误码组件
 *
 * 将错误码缓存在内存中，同时通过定时器每 n 分钟更新
 */
package com.syj.boot.adminserver.modules.tool.framework.errorcode;
