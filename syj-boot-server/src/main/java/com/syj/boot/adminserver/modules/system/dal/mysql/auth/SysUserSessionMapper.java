package com.syj.boot.adminserver.modules.system.dal.mysql.auth;

import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.qdp.framework.mybatis.core.mapper.BaseMapperX;
import com.syj.qdp.framework.mybatis.core.query.QueryWrapperX;
import com.syj.boot.adminserver.modules.system.controller.auth.vo.session.SysUserSessionPageReqVO;
import com.syj.boot.adminserver.modules.system.dal.dataobject.auth.SysUserSessionDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Mapper
public interface SysUserSessionMapper extends BaseMapperX<SysUserSessionDO> {

    default PageResult<SysUserSessionDO> selectPage(SysUserSessionPageReqVO reqVO, Collection<Long> userIds) {
        return selectPage(reqVO, new QueryWrapperX<SysUserSessionDO>()
                .inIfPresent("user_id", userIds)
                .likeIfPresent("user_ip", reqVO.getUserIp()));
    }

    default List<SysUserSessionDO> selectListBySessionTimoutLt() {
        return selectList(new QueryWrapperX<SysUserSessionDO>().lt("session_timeout",new Date()));
    }
}
