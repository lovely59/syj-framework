package com.syj.boot.adminserver.modules.system.convert.errorcode;

import com.syj.boot.adminserver.modules.system.dal.dataobject.errorcode.SysErrorCodeDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.boot.adminserver.modules.tool.framework.errorcode.core.dto.ErrorCodeAutoGenerateReqDTO;
import com.syj.boot.adminserver.modules.tool.framework.errorcode.core.dto.ErrorCodeRespDTO;
import com.syj.boot.adminserver.modules.system.controller.errorcode.vo.SysErrorCodeCreateReqVO;
import com.syj.boot.adminserver.modules.system.controller.errorcode.vo.SysErrorCodeExcelVO;
import com.syj.boot.adminserver.modules.system.controller.errorcode.vo.SysErrorCodeRespVO;
import com.syj.boot.adminserver.modules.system.controller.errorcode.vo.SysErrorCodeUpdateReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 错误码 Convert
 *
 * @author Lyon
 */
@Mapper
public interface SysErrorCodeConvert {

    SysErrorCodeConvert INSTANCE = Mappers.getMapper(SysErrorCodeConvert.class);

    SysErrorCodeDO convert(SysErrorCodeCreateReqVO bean);

    SysErrorCodeDO convert(SysErrorCodeUpdateReqVO bean);

    SysErrorCodeRespVO convert(SysErrorCodeDO bean);

    List<SysErrorCodeRespVO> convertList(List<SysErrorCodeDO> list);

    PageResult<SysErrorCodeRespVO> convertPage(PageResult<SysErrorCodeDO> page);

    List<SysErrorCodeExcelVO> convertList02(List<SysErrorCodeDO> list);

    SysErrorCodeDO convert(ErrorCodeAutoGenerateReqDTO bean);

    List<ErrorCodeRespDTO> convertList03(List<SysErrorCodeDO> list);

}
