package com.syj.boot.adminserver.modules.system.convert.auth;

import com.syj.boot.adminserver.modules.system.controller.auth.vo.session.SysUserSessionPageItemRespVO;
import com.syj.boot.adminserver.modules.system.dal.dataobject.auth.SysUserSessionDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SysUserSessionConvert {

    SysUserSessionConvert INSTANCE = Mappers.getMapper(SysUserSessionConvert.class);

    SysUserSessionPageItemRespVO convert(SysUserSessionDO session);

}
