package com.syj.boot.adminserver.modules.infra.dal.mysql.file;

import com.syj.boot.adminserver.modules.infra.controller.file.vo.InfFilePageReqVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.file.InfFileDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.qdp.framework.mybatis.core.mapper.BaseMapperX;
import com.syj.qdp.framework.mybatis.core.query.QueryWrapperX;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InfFileMapper extends BaseMapperX<InfFileDO> {

    default Integer selectCountById(String id) {
        return selectCount("id", id);
    }

    default PageResult<InfFileDO> selectPage(InfFilePageReqVO reqVO) {
        return selectPage(reqVO, new QueryWrapperX<InfFileDO>()
                .likeIfPresent("id", reqVO.getId())
                .likeIfPresent("type", reqVO.getType())
                .betweenIfPresent("create_time", reqVO.getBeginCreateTime(), reqVO.getEndCreateTime())
                .orderByDesc("create_time"));
    }

}
