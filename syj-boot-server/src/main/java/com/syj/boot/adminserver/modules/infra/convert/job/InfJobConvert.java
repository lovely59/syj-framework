package com.syj.boot.adminserver.modules.infra.convert.job;

import com.syj.boot.adminserver.modules.infra.controller.job.vo.job.InfJobCreateReqVO;
import com.syj.boot.adminserver.modules.infra.controller.job.vo.job.InfJobExcelVO;
import com.syj.boot.adminserver.modules.infra.controller.job.vo.job.InfJobRespVO;
import com.syj.boot.adminserver.modules.infra.controller.job.vo.job.InfJobUpdateReqVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.job.InfJobDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 定时任务 Convert
 *
 * @author Lyon
 */
@Mapper
public interface InfJobConvert {

    InfJobConvert INSTANCE = Mappers.getMapper(InfJobConvert.class);

    InfJobDO convert(InfJobCreateReqVO bean);

    InfJobDO convert(InfJobUpdateReqVO bean);

    InfJobRespVO convert(InfJobDO bean);

    List<InfJobRespVO> convertList(List<InfJobDO> list);

    PageResult<InfJobRespVO> convertPage(PageResult<InfJobDO> page);

    List<InfJobExcelVO> convertList02(List<InfJobDO> list);

}
