package com.syj.boot.adminserver.modules.infra.dal.mysql.job;

import com.syj.boot.adminserver.modules.infra.controller.job.vo.job.InfJobExportReqVO;
import com.syj.boot.adminserver.modules.infra.controller.job.vo.job.InfJobPageReqVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.job.InfJobDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.qdp.framework.mybatis.core.mapper.BaseMapperX;
import com.syj.qdp.framework.mybatis.core.query.QueryWrapperX;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 定时任务 Mapper
 *
 * @author Lyon
 */
@Mapper
public interface InfJobMapper extends BaseMapperX<InfJobDO> {

    default InfJobDO selectByHandlerName(String handlerName) {
        return selectOne("handler_name", handlerName);
    }

    default PageResult<InfJobDO> selectPage(InfJobPageReqVO reqVO) {
        return selectPage(reqVO, new QueryWrapperX<InfJobDO>()
                .likeIfPresent("name", reqVO.getName())
                .eqIfPresent("status", reqVO.getStatus())
                .likeIfPresent("handler_name", reqVO.getHandlerName())
        );
    }

    default List<InfJobDO> selectList(InfJobExportReqVO reqVO) {
        return selectList(new QueryWrapperX<InfJobDO>()
                .likeIfPresent("name", reqVO.getName())
                .eqIfPresent("status", reqVO.getStatus())
                .likeIfPresent("handler_name", reqVO.getHandlerName())
        );
    }

}
