package com.syj.boot.adminserver.framework.monitor.config;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.context.annotation.Configuration;

/**
 * @author lyon
 */
@Configuration
@EnableAdminServer
public class AdminServerConfiguration {
}
