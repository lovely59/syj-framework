package com.syj.boot.adminserver.modules.system.controller.dept.vo.post;

import com.syj.qdp.framework.common.pojo.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel("岗位分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
public class SysPostPageReqVO extends PageParam {

    @ApiModelProperty(value = "岗位编码", example = "admin", notes = "模糊匹配")
    private String code;

    @ApiModelProperty(value = "岗位名称", example = "Lyon", notes = "模糊匹配")
    private String name;

    @ApiModelProperty(value = "展示状态", example = "1", notes = "参见 SysCommonStatusEnum 枚举类")
    private Integer status;

}
