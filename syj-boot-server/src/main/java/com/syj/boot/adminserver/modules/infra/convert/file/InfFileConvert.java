package com.syj.boot.adminserver.modules.infra.convert.file;

import com.syj.boot.adminserver.modules.infra.controller.file.vo.InfFileRespVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.file.InfFileDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface InfFileConvert {

    InfFileConvert INSTANCE = Mappers.getMapper(InfFileConvert.class);

    InfFileRespVO convert(InfFileDO bean);

    PageResult<InfFileRespVO> convertPage(PageResult<InfFileDO> page);

}
