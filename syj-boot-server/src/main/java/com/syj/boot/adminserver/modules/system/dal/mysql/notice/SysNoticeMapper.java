package com.syj.boot.adminserver.modules.system.dal.mysql.notice;

import com.syj.boot.adminserver.modules.system.dal.dataobject.notice.SysNoticeDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.qdp.framework.mybatis.core.mapper.BaseMapperX;
import com.syj.qdp.framework.mybatis.core.query.QueryWrapperX;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticePageReqVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysNoticeMapper extends BaseMapperX<SysNoticeDO> {

    default PageResult<SysNoticeDO> selectPage(SysNoticePageReqVO reqVO) {
        return selectPage(reqVO, new QueryWrapperX<SysNoticeDO>()
                .likeIfPresent("title", reqVO.getTitle())
                .eqIfPresent("status", reqVO.getStatus()));
    }

}
