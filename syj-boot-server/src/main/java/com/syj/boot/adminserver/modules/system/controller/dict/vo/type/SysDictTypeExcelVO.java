package com.syj.boot.adminserver.modules.system.controller.dict.vo.type;

import com.syj.boot.adminserver.modules.system.enums.SysDictTypeConstants;
import com.syj.qdp.framework.excel.core.annotations.DictFormat;
import com.syj.qdp.framework.excel.core.convert.DictConvert;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 字典类型 Excel 导出响应 VO
 */
@Data
public class SysDictTypeExcelVO {

    @ExcelProperty("字典主键")
    private Long id;

    @ExcelProperty("字典名称")
    private String name;

    @ExcelProperty("字典类型")
    private String type;

    @ExcelProperty(value = "状态", converter = DictConvert.class)
    @DictFormat(SysDictTypeConstants.COMMON_STATUS)
    private Integer status;

}
