package com.syj.boot.adminserver.modules.infra.convert.logger;

import com.syj.boot.adminserver.modules.infra.controller.logger.vo.apierrorlog.InfApiErrorLogExcelVO;
import com.syj.boot.adminserver.modules.infra.controller.logger.vo.apierrorlog.InfApiErrorLogRespVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.logger.InfApiErrorLogDO;
import com.syj.qdp.framework.apilog.core.service.dto.ApiErrorLogCreateDTO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * API 错误日志 Convert
 *
 * @author Lyon
 */
@Mapper
public interface InfApiErrorLogConvert {

    InfApiErrorLogConvert INSTANCE = Mappers.getMapper(InfApiErrorLogConvert.class);

    InfApiErrorLogDO convert(ApiErrorLogCreateDTO bean);

    InfApiErrorLogRespVO convert(InfApiErrorLogDO bean);

    PageResult<InfApiErrorLogRespVO> convertPage(PageResult<InfApiErrorLogDO> page);

    List<InfApiErrorLogExcelVO> convertList02(List<InfApiErrorLogDO> list);

}
