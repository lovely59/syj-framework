package com.syj.boot.adminserver.framework.async.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author lyon
 */
@Configuration
@EnableAsync(proxyTargetClass = true)
public class AsyncConfiguration {
}
