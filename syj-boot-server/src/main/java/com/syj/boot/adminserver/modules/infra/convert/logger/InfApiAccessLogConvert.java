package com.syj.boot.adminserver.modules.infra.convert.logger;

import com.syj.boot.adminserver.modules.infra.controller.logger.vo.apiaccesslog.InfApiAccessLogExcelVO;
import com.syj.boot.adminserver.modules.infra.controller.logger.vo.apiaccesslog.InfApiAccessLogRespVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.logger.InfApiAccessLogDO;
import com.syj.qdp.framework.apilog.core.service.dto.ApiAccessLogCreateDTO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * API 访问日志 Convert
 *
 * @author Lyon
 */
@Mapper
public interface InfApiAccessLogConvert {

    InfApiAccessLogConvert INSTANCE = Mappers.getMapper(InfApiAccessLogConvert.class);

    InfApiAccessLogDO convert(ApiAccessLogCreateDTO bean);

    InfApiAccessLogRespVO convert(InfApiAccessLogDO bean);

    List<InfApiAccessLogRespVO> convertList(List<InfApiAccessLogDO> list);

    PageResult<InfApiAccessLogRespVO> convertPage(PageResult<InfApiAccessLogDO> page);

    List<InfApiAccessLogExcelVO> convertList02(List<InfApiAccessLogDO> list);

}
