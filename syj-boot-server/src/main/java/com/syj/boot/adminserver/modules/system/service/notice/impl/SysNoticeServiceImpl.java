package com.syj.boot.adminserver.modules.system.service.notice.impl;

import com.syj.boot.adminserver.modules.system.dal.dataobject.notice.SysNoticeDO;
import com.syj.boot.adminserver.modules.system.dal.mysql.notice.SysNoticeMapper;
import com.syj.boot.adminserver.modules.system.enums.SysErrorCodeConstants;
import com.syj.boot.adminserver.modules.system.service.notice.SysNoticeService;
import com.syj.qdp.framework.common.exception.util.ServiceExceptionUtil;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticeCreateReqVO;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticePageReqVO;
import com.syj.boot.adminserver.modules.system.controller.notice.vo.SysNoticeUpdateReqVO;
import com.syj.boot.adminserver.modules.system.convert.notice.SysNoticeConvert;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 通知公告 Service 实现类
 *
 * @author Lyon
 */
@Service
public class SysNoticeServiceImpl implements SysNoticeService {

    @Resource
    private SysNoticeMapper noticeMapper;

    @Override
    public Long createNotice(SysNoticeCreateReqVO reqVO) {
        SysNoticeDO notice = SysNoticeConvert.INSTANCE.convert(reqVO);
        noticeMapper.insert(notice);
        return notice.getId();
    }

    @Override
    public void updateNotice(SysNoticeUpdateReqVO reqVO) {
        // 校验是否存在
        this.checkNoticeExists(reqVO.getId());
        // 更新通知公告
        SysNoticeDO updateObj = SysNoticeConvert.INSTANCE.convert(reqVO);
        noticeMapper.updateById(updateObj);
    }

    @Override
    public void deleteNotice(Long id) {
        // 校验是否存在
        this.checkNoticeExists(id);
        // 删除通知公告
        noticeMapper.deleteById(id);
    }

    @Override
    public PageResult<SysNoticeDO> pageNotices(SysNoticePageReqVO reqVO) {
        return noticeMapper.selectPage(reqVO);
    }

    @Override
    public SysNoticeDO getNotice(Long id) {
        return noticeMapper.selectById(id);
    }

    @VisibleForTesting
    public void checkNoticeExists(Long id) {
        if (id == null) {
            return;
        }
        SysNoticeDO notice = noticeMapper.selectById(id);
        if (notice == null) {
            throw ServiceExceptionUtil.exception(SysErrorCodeConstants.NOTICE_NOT_FOUND);
        }
    }

}
