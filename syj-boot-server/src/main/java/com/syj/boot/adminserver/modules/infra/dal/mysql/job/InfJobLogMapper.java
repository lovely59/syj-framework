package com.syj.boot.adminserver.modules.infra.dal.mysql.job;

import com.syj.boot.adminserver.modules.infra.controller.job.vo.log.InfJobLogExportReqVO;
import com.syj.boot.adminserver.modules.infra.controller.job.vo.log.InfJobLogPageReqVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.job.InfJobLogDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.qdp.framework.mybatis.core.mapper.BaseMapperX;
import com.syj.qdp.framework.mybatis.core.query.QueryWrapperX;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 任务日志 Mapper
 *
 * @author Lyon
 */
@Mapper
public interface InfJobLogMapper extends BaseMapperX<InfJobLogDO> {

    default PageResult<InfJobLogDO> selectPage(InfJobLogPageReqVO reqVO) {
        return selectPage(reqVO, new QueryWrapperX<InfJobLogDO>()
                .eqIfPresent("job_id", reqVO.getJobId())
                .likeIfPresent("handler_name", reqVO.getHandlerName())
                .geIfPresent("begin_time", reqVO.getBeginTime())
                .leIfPresent("end_time", reqVO.getEndTime())
                .eqIfPresent("status", reqVO.getStatus())
                .orderByDesc("id") // ID 倒序
        );
    }

    default List<InfJobLogDO> selectList(InfJobLogExportReqVO reqVO) {
        return selectList(new QueryWrapperX<InfJobLogDO>()
                .eqIfPresent("job_id", reqVO.getJobId())
                .likeIfPresent("handler_name", reqVO.getHandlerName())
                .geIfPresent("begin_time", reqVO.getBeginTime())
                .leIfPresent("end_time", reqVO.getEndTime())
                .eqIfPresent("status", reqVO.getStatus())
                .orderByDesc("id") // ID 倒序
        );
    }

}
