package com.syj.boot.adminserver.modules.infra.convert.job;

import com.syj.boot.adminserver.modules.infra.controller.job.vo.log.InfJobLogExcelVO;
import com.syj.boot.adminserver.modules.infra.controller.job.vo.log.InfJobLogRespVO;
import com.syj.boot.adminserver.modules.infra.dal.dataobject.job.InfJobLogDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 定时任务日志 Convert
 *
 * @author Lyon
 */
@Mapper
public interface InfJobLogConvert {

    InfJobLogConvert INSTANCE = Mappers.getMapper(InfJobLogConvert.class);

    InfJobLogRespVO convert(InfJobLogDO bean);

    List<InfJobLogRespVO> convertList(List<InfJobLogDO> list);

    PageResult<InfJobLogRespVO> convertPage(PageResult<InfJobLogDO> page);

    List<InfJobLogExcelVO> convertList02(List<InfJobLogDO> list);

}
