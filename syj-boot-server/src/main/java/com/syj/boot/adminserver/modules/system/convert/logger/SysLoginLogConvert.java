package com.syj.boot.adminserver.modules.system.convert.logger;

import com.syj.boot.adminserver.modules.system.dal.dataobject.logger.SysLoginLogDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import com.syj.boot.adminserver.modules.system.controller.logger.vo.loginlog.SysLoginLogCreateReqVO;
import com.syj.boot.adminserver.modules.system.controller.logger.vo.loginlog.SysLoginLogExcelVO;
import com.syj.boot.adminserver.modules.system.controller.logger.vo.loginlog.SysLoginLogRespVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SysLoginLogConvert {

    SysLoginLogConvert INSTANCE = Mappers.getMapper(SysLoginLogConvert.class);

    SysLoginLogDO convert(SysLoginLogCreateReqVO bean);

    PageResult<SysLoginLogRespVO> convertPage(PageResult<SysLoginLogDO> page);

    List<SysLoginLogExcelVO> convertList(List<SysLoginLogDO> list);

}
