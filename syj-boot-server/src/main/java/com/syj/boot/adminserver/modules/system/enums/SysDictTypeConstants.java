package com.syj.boot.adminserver.modules.system.enums;

/**
 * System 字典类型的枚举类
 *
 * @author Lyon
 */
public interface SysDictTypeConstants {

    String USER_TYPE = "user_type"; // 用户类型
    String COMMON_STATUS = "sys_common_status"; // 系统状态

    String USER_SEX = "sys_user_sex"; // 用户性别
    String OPERATE_TYPE = "sys_operate_type"; // 操作类型
    String LOGIN_RESULT = "sys_login_result"; // 登陆结果
    String CONFIG_TYPE = "sys_config_type"; // 参数配置类型
    String BOOLEAN_STRING = "sys_boolean_string"; // Boolean 是否类型


}
