package com.syj.boot.adminserver.modules.system.convert.dict;

import com.syj.boot.adminserver.modules.system.controller.dict.vo.type.*;
import com.syj.boot.adminserver.modules.system.dal.dataobject.dict.SysDictTypeDO;
import com.syj.qdp.framework.common.pojo.PageResult;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SysDictTypeConvert {

    SysDictTypeConvert INSTANCE = Mappers.getMapper(SysDictTypeConvert.class);

    PageResult<SysDictTypeRespVO> convertPage(PageResult<SysDictTypeDO> bean);

    SysDictTypeRespVO convert(SysDictTypeDO bean);

    SysDictTypeDO convert(SysDictTypeCreateReqVO bean);

    SysDictTypeDO convert(SysDictTypeUpdateReqVO bean);

    List<SysDictTypeSimpleRespVO> convertList(List<SysDictTypeDO> list);

    List<SysDictTypeExcelVO> convertList02(List<SysDictTypeDO> list);

}
