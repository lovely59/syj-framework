package com.syj.boot.adminserver.modules.tool.enums;

/**
 * Tool 字典类型的枚举类
 *
 * @author Lyon
 */
public interface ToolDictTypeConstants {

    String TEST_DEMO_TYPE = "tool_test_demo_type";

}
