package com.syj.qdp.framework.skywalking.core.annotations;

import java.lang.annotation.*;

/**
 * 打印业务编号 / 业务类型注解
 * <p>
 * 使用时，需要设置 SkyWalking OAP Server 的 application.yaml 配置文件，修改 SW_SEARCHABLE_TAG_KEYS 配置项，
 * 增加 biz.type 和 biz.id 两值，然后重启 SkyWalking OAP Server 服务器。
 *
 * @author Lyon
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface BizTrace {

    String SAPN_TYPE = "biz";
    /**
     * 业务编号 tag 名
     */
    String ID_TAG = "biz.id";
    /**
     * 业务类型 tag 名
     */
    String TYPE_TAG = "biz.type";


    /**
     * 操作名称
     */
    String operationName() default "";

    /**
     * 业务类型
     *
     * @return
     */
    String bizType();

    /**
     * 业务编号
     *
     * @return
     */
    String bizId();

}
