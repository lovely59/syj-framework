package com.syj.qdp.framework.skywalking.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Lyon
 */
@ConfigurationProperties("syj.qdp.tracer")
@Data
public class TracerProperties {
    /**
     * 链路追踪是否开启，默认false
     */
    private boolean enable = false;
}
