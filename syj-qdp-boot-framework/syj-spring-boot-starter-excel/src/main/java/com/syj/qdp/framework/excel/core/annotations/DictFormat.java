package com.syj.qdp.framework.excel.core.annotations;

import java.lang.annotation.*;

/**
 * excel导出时 字典表转换
 * @author Lyon
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@Documented
public @interface DictFormat {

    /**
     * 字典表的 key
     * @return
     */
    String value();

}
