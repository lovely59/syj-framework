package com.syj.qdp.framework.protection.lock4j.config;

import com.baomidou.lock.LockFailureStrategy;
import com.syj.qdp.framework.protection.lock4j.core.DefaultLockFailureStrategy;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * @author Lyon
 */
public class Lock4JAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public LockFailureStrategy defaultFailureStrategy(){
        return new DefaultLockFailureStrategy();
    }

}
