package com.syj.qdp.framework.protection.idempotent.core.resovler.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.syj.qdp.framework.protection.idempotent.core.annotations.Idempotent;
import com.syj.qdp.framework.protection.idempotent.core.resovler.IdempotentKeyResolver;
import org.aspectj.lang.JoinPoint;

/**
 * @author Lyon
 */
public class StandardIdempotentKeyResolver implements IdempotentKeyResolver {
    @Override
    public String bulidKey(JoinPoint point, Idempotent idempotent) {
        String methodName = point.getSignature().toString();
        String args = StrUtil.join(",", point.getArgs());
        return SecureUtil.md5(methodName.concat(args));
    }
}
