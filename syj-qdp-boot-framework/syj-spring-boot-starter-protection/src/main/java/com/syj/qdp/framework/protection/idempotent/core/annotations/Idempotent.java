package com.syj.qdp.framework.protection.idempotent.core.annotations;


import com.syj.qdp.framework.protection.idempotent.core.resovler.IdempotentKeyResolver;
import com.syj.qdp.framework.protection.idempotent.core.resovler.impl.ExpressionIdempotentKeyResolver;
import com.syj.qdp.framework.protection.idempotent.core.resovler.impl.StandardIdempotentKeyResolver;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 幂等控制注解
 *
 * @author lyon
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Idempotent {

    /**使用SPEL表达式解析*/
    String keyArgs() default "";

    String message() default "重复操作，请稍后再试！";

    /**
     * key解析器
     * spel表达式 #{@link ExpressionIdempotentKeyResolver}
     * toString #{@link StandardIdempotentKeyResolver}
     *
     * @return
     */
    Class<? extends IdempotentKeyResolver> resolver() default StandardIdempotentKeyResolver.class;

    /**
     * 超时时间
     */
    long timeout() default 2;

    /**
     * 时间单位
     */
    TimeUnit unit() default TimeUnit.SECONDS;

}
