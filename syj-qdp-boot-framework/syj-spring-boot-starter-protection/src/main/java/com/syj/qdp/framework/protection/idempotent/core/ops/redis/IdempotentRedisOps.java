package com.syj.qdp.framework.protection.idempotent.core.ops.redis;

import cn.hutool.core.util.IdUtil;
import com.syj.qdp.framework.protection.idempotent.core.annotations.Idempotent;
import com.syj.qdp.framework.protection.idempotent.core.ops.IdempotentOps;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

/**
 * @author Lyon
 */
@RequiredArgsConstructor
public class IdempotentRedisOps implements IdempotentOps {

    final RedisTemplate<String, Object> redisTemplate;

    @Override
    public boolean setIfAbsent(String key, Idempotent idempotent) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        return operations.setIfAbsent(formatKey(key), IdUtil.fastUUID(), idempotent.timeout(), idempotent.unit());
    }

}
