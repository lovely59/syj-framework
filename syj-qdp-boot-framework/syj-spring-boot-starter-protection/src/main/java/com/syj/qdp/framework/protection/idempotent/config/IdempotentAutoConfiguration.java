package com.syj.qdp.framework.protection.idempotent.config;

import com.syj.qdp.framework.protection.idempotent.core.aop.IdempotentAspect;
import com.syj.qdp.framework.protection.idempotent.core.ops.IdempotentOps;
import com.syj.qdp.framework.protection.idempotent.core.ops.redis.IdempotentRedisOps;
import com.syj.qdp.framework.protection.idempotent.core.resovler.IdempotentKeyResolver;
import com.syj.qdp.framework.protection.idempotent.core.resovler.impl.ExpressionIdempotentKeyResolver;
import com.syj.qdp.framework.protection.idempotent.core.resovler.impl.StandardIdempotentKeyResolver;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

/**
 * @author Lyon
 */
public class IdempotentAutoConfiguration {

    @Bean
    @ConditionalOnBean({
            IdempotentKeyResolver.class,
            RedisTemplate.class
    })
    @ConditionalOnMissingBean
    IdempotentAspect idempotentAspect(List<IdempotentKeyResolver> resolvers , IdempotentOps idempotentOps){
        return new IdempotentAspect(resolvers,idempotentOps);
    }

    @Bean
    IdempotentOps idempotentOps(RedisTemplate<String, Object> redisTemplate){
        return new IdempotentRedisOps(redisTemplate);
    }

    @Bean
    IdempotentKeyResolver standardIdempotentKeyResolver(){
        return new StandardIdempotentKeyResolver();
    }

    @Bean
    IdempotentKeyResolver expressionIdempotentKeyResolver(){
        return new ExpressionIdempotentKeyResolver();
    }


}
