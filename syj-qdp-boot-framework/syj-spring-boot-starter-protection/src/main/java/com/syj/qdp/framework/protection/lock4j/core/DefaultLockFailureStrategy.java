package com.syj.qdp.framework.protection.lock4j.core;

import com.baomidou.lock.LockFailureStrategy;
import com.syj.qdp.framework.common.exception.enums.GlobalErrorCodeConstants;
import com.syj.qdp.framework.common.exception.util.ServiceExceptionUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Lyon
 */
@Slf4j
public class DefaultLockFailureStrategy implements LockFailureStrategy {

    @Override
    public void onLockFailure(String key, long acquireTimeout, int acquireCount) {
        log.debug("[onLockFailure] 获取锁失败 线程[{} ]key:[{}] 超时时长:[{}] 获取次数:[{}]",Thread.currentThread().getName(),key,acquireTimeout,acquireCount);
        throw ServiceExceptionUtil.exception(GlobalErrorCodeConstants.LOCKED);
    }
}
