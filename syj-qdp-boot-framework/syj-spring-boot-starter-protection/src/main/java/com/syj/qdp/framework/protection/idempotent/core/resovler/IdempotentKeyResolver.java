package com.syj.qdp.framework.protection.idempotent.core.resovler;

import com.syj.qdp.framework.protection.idempotent.core.annotations.Idempotent;
import org.aspectj.lang.JoinPoint;

/**
 * @author Lyon
 */
public interface IdempotentKeyResolver {

    /**
     * 生成key
     * @param point
     * @param idempotent
     * @return
     */
    String bulidKey(JoinPoint point , Idempotent idempotent);

}
