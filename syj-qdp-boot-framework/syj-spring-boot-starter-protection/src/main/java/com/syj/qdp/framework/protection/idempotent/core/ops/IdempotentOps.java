package com.syj.qdp.framework.protection.idempotent.core.ops;

import cn.hutool.core.util.StrUtil;
import com.syj.qdp.framework.protection.idempotent.core.annotations.Idempotent;

/**
 * @author Lyon
 */
public interface IdempotentOps {

    /**
     * 如果未存在幂等限制，就设置
     *
     * @param key
     * @return 是否成功设置
     */
    boolean setIfAbsent(String key, Idempotent idempotent);

    class IdempotentTemplate {
        public static String value = "idempotent:[{}]";
    }

    default String formatKey(String key){
        return StrUtil.format(IdempotentTemplate.value,key);
    }

}
