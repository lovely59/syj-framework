package com.syj.qdp.framework.mybatis.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * 使用自动配置时，必须有的mapper-base-package属性
 * 如果不需要自动配置，请禁用 {@link MybatisAutoConfiguration}
 * @author Lyon
 */
@ConfigurationProperties("syj.qdp.data")
@Data
@Validated
public class MybatisProperties {

    @NotBlank(message = "mapper基础扫描包不能为空")
    private String basePackage;

    private boolean autoScanner;

}
