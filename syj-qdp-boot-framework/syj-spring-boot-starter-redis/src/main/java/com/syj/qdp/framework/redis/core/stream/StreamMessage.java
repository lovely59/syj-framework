package com.syj.qdp.framework.redis.core.stream;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Lyon
 */
public interface StreamMessage {

    @JsonIgnore
    String getStreamKey();

}
