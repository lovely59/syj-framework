package com.syj.qdp.framework.redis.core.cache;

import cn.hutool.core.collection.CollUtil;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author Lyon
 */
public class DefaultRedisCacheTemplate implements RedisCacheTemplate {

    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @Override
    public Boolean expire(String key, Long time) {
        return this.redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    @Override
    public Long getExpire(String key) {
        return this.redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    @Override
    public Boolean hasKey(String key) {
        return this.redisTemplate.hasKey(key);
    }

    @Override
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                this.redisTemplate.delete(key[0]);
            } else {
                this.redisTemplate.delete(CollUtil.newArrayList(key));
            }
        }

    }

    @Override
    public Object get(String key) {
        return key == null ? null : this.redisTemplate.opsForValue().get(key);
    }

    @Override
    public void set(String key, Object value) {
        try {
            this.redisTemplate.opsForValue().set(key, value);
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }

    @Override
    public Boolean set(String key, Object value, Long time) {
        try {
            if (time > 0L) {
                this.redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                this.set(key, value);
            }

            return true;
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }
    }

    @Override
    public Long increment(String key, Long incrementValue) {
        if (incrementValue > 0L) {
            throw new RuntimeException("increment参数必须大于0");
        } else {
            return this.redisTemplate.opsForValue().increment(key, incrementValue);
        }
    }

    @Override
    public Long decrement(String key, Long incrementValue) {
        if (incrementValue > 0L) {
            throw new RuntimeException("increment参数必须大于0");
        } else {
            return this.redisTemplate.opsForValue().increment(key, -incrementValue);
        }
    }

    @Override
    public Object hget(String key, String item) {
        return this.redisTemplate.opsForHash().get(key, item);
    }

    @Override
    public Map<Object, Object> hmget(String key) {
        return this.redisTemplate.opsForHash().entries(key);
    }

    @Override
    public Boolean hmset(String key, Map<String, Object> map) {
        try {
            this.redisTemplate.opsForHash().putAll(key, map);
        } catch (Exception var4) {
            var4.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Boolean hmset(String key, Map<String, Object> map, Long time) {
        try {
            this.redisTemplate.opsForHash().putAll(key, map);
            if (time > 0L) {
                this.expire(key, time);
            }
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Boolean hset(String key, String item, Object value) {
        try {
            this.redisTemplate.opsForHash().put(key, item, value);
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Boolean hset(String key, String item, Object value, Long time) {
        try {
            this.redisTemplate.opsForHash().put(key, item, value);
            if (time > 0L) {
                this.expire(key, time);
            }
        } catch (Exception var6) {
            var6.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public void hdel(String key, Object... item) {
        this.redisTemplate.opsForHash().delete(key, item);
    }

    @Override
    public Boolean hHasKey(String key, String item) {
        return this.redisTemplate.opsForHash().hasKey(key, item);
    }

    @Override
    public Set<Object> sGet(String key) {
        return this.redisTemplate.opsForSet().members(key);
    }

    @Override
    public Boolean sHasKey(String key, Object value) {
        return this.redisTemplate.opsForSet().isMember(key, value);
    }

    @Override
    public Long sSet(String key, Object... values) {
        return this.redisTemplate.opsForSet().add(key, values);
    }

    @Override
    public Long sSet(String key, Long time, Object... values) {
        return this.redisTemplate.opsForSet().add(key, values);
    }

    @Override
    public Long sSize(String key) {
        return this.redisTemplate.opsForSet().size(key);
    }

    @Override
    public Long sRemove(String key, Object... values) {
        return this.redisTemplate.opsForSet().remove(key, values);
    }

    @Override
    public List<Object> lRange(String key, Long start, Long end) {
        try {
            return this.redisTemplate.opsForList().range(key, start, end);
        } catch (Exception var5) {
            var5.printStackTrace();
            return null;
        }
    }

    @Override
    public Long lSize(String key) {
        return this.redisTemplate.opsForList().size(key);
    }

    @Override
    public Object lIndex(String key, Long index) {
        return this.redisTemplate.opsForList().index(key, index);
    }

    @Override
    public Boolean lSet(String key, Object value) {
        try {
            this.redisTemplate.opsForList().rightPush(key, value);
        } catch (Exception var4) {
            var4.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Boolean lSet(String key, Object value, Long time) {
        try {
            this.redisTemplate.opsForList().rightPush(key, value);
            if (time > 0L) {
                this.expire(key, time);
            }
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Boolean lSet(String key, List<Object> value) {
        try {
            this.redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception var4) {
            var4.printStackTrace();
            return false;
        }
    }

    @Override
    public Boolean lSet(String key, List<Object> value, Long time) {
        try {
            this.redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0L) {
                this.expire(key, time);
            }
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public Boolean lUpdateIndex(String key, Long index, Object value) {
        try {
            this.redisTemplate.opsForList().set(key, index, value);
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }

        return true;
    }
}
