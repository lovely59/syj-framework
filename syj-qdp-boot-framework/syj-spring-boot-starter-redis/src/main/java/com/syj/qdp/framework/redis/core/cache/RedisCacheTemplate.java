package com.syj.qdp.framework.redis.core.cache;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Lyon
 */
public interface RedisCacheTemplate {

    Boolean expire(String key, Long time);

    Long getExpire(String key);

    Boolean hasKey(String key);

    void del(String... key);

    Object get(String key);

    void set(String key, Object value);

    Boolean set(String key, Object value, Long time);

    Long increment(String key, Long incrementValue);

    Long decrement(String key, Long incrementValue);

    Object hget(String key, String item);

    Map<Object, Object> hmget(String key);

    Boolean hmset(String key, Map<String, Object> map);

    Boolean hmset(String key, Map<String, Object> map, Long time);

    Boolean hset(String key, String item, Object value);

    Boolean hset(String key, String item, Object value, Long time);

    void hdel(String key, Object... item);

    Boolean hHasKey(String key, String item);

    Set<Object> sGet(String key);

    Boolean sHasKey(String key, Object value);

    Long sSet(String key, Object... values);

    Long sSet(String key, Long time, Object... values);

    Long sSize(String key);

    Long sRemove(String key, Object... values);

    List<Object> lRange(String key, Long start, Long end);

    Long lSize(String key);

    Object lIndex(String key, Long index);

    Boolean lSet(String key, Object value);

    Boolean lSet(String key, Object value, Long time);

    Boolean lSet(String key, List<Object> value);

    Boolean lSet(String key, List<Object> value, Long time);

    Boolean lUpdateIndex(String key, Long index, Object value);

}
