package com.syj.qdp.framework.redis.core.pubsub;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Lyon
 */
@FunctionalInterface
public interface ChannelMessage {

    /**
     * @return redis pub/sub channel
     */
    @JsonIgnore
    String getChannel();

}
