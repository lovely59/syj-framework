package com.syj.qdp.framework.quartz.config;

import com.syj.qdp.framework.quartz.core.scheduler.SchedulerManager;
import org.quartz.Scheduler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Lyon
 */
@EnableScheduling
public class QuartzAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    SchedulerManager schedulerManager(Scheduler scheduler) {
        return new SchedulerManager(scheduler);
    }

}
