package com.syj.qdp.framework.quartz.core.handler;

/**
 * @author Lyon
 */
public interface JobHandler {

    /**
     * 执行任务
     *
     * @param param 参数
     * @return 结果
     * @throws Exception 异常
     */
    String execute(String param) throws Exception;

}
