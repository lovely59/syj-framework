package com.syj.qdp.framework.quartz.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * job 枚举
 *
 * @author Lyon
 */
@AllArgsConstructor
@Getter
public enum JobDataKeyEnum {

    /**
     * 任务标识
     */
    JOB_ID,
    /**
     * 任务名称
     */
    JOB_HANDLER_NAME,
    /**
     * 参数
     */
    JOB_HANDLER_PARAM,
    /**
     * 最大重试次数
     */
    JOB_RETRY_COUNT,
    /**
     * 每次重试间隔
     */
    JOB_RETRY_INTERVAL;


}
