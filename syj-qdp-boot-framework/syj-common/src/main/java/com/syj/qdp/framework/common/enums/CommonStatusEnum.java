package com.syj.qdp.framework.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 通用枚举状态
 *
 * @author Lyon
 */
@Getter
@AllArgsConstructor
public enum CommonStatusEnum {
    /**
     * 通用状态枚举
     * */
    ENABLE(0,"开启"),
    DISABLE(1,"关闭");

    private final Integer status;
    private final String name;

}
