package com.syj.qdp.framework.common.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 分页模型
 *
 * @author Lyon
 */
@Data
@ApiModel("分页模型")
public class PageParam implements Serializable {

    static final Integer DEFAULT_PAGE_NO = 1;
    static final Integer DEFAULT_PAGE_SIZE = 10;

    @ApiModelProperty("页码")
    @Min(value = 1, message = "页码最小值为1")
    @NotNull(message = "页码不能为空")
    private Integer pageNo = DEFAULT_PAGE_NO;

    @ApiModelProperty("每页条数")
    @Min(value = 10, message = "每页条数最小值为10")
    @Max(value = 100, message = "每页条数最大值为100")
    @NotNull(message = "每页条数不能为空")
    private Integer pageSize = DEFAULT_PAGE_SIZE;

}
