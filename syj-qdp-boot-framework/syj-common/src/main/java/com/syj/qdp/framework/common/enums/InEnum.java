package com.syj.qdp.framework.common.enums;


import com.syj.qdp.framework.common.core.IntArrayValuable;
import com.syj.qdp.framework.common.validation.InEnumValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author Lyon
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(
        {
                ElementType.PARAMETER,
                ElementType.FIELD,
                ElementType.CONSTRUCTOR,
                ElementType.TYPE_USE,
                ElementType.TYPE_PARAMETER
        }
)
@Documented
@Inherited
@Constraint(validatedBy = InEnumValidator.class)
public @interface InEnum {

    Class<IntArrayValuable> value();

    String message() default "必须在指定范围内{}";

    String group() default "";

    Class<? extends Payload>[] payload();

}
