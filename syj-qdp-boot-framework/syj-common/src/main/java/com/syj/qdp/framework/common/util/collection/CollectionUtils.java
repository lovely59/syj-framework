package com.syj.qdp.framework.common.util.collection;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import javax.annotation.MatchesPattern;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Lyon
 */
public class CollectionUtils {


    public static boolean containsAny(Object source, Object... targets) {
        return Arrays.asList(targets).contains(source);
    }

    public static boolean isAnyEmpty(Collection<?>... collections) {
        return Arrays.stream(collections).anyMatch(CollectionUtil::isEmpty);
    }
    public static <T> boolean isEmpty(Collection<T> from){
        return from == null || from.isEmpty();
    }

    public static <T> boolean notEmpty(Collection<T> from){
        return !isEmpty(from);
    }

    public static <T> List<T> filterList(Collection<T> from, Predicate<T> predicate) {
        if (CollUtil.isEmpty(from)) {
            return new ArrayList<>();
        }
        return from.stream().filter(predicate).collect(Collectors.toList());
    }

    public static <T, K> Map<K, T> convertMap(Collection<T> from, Function<T, K> keyFunc) {
        if (CollUtil.isEmpty(from)) {
            return new HashMap<>();
        }
        return convertMap(from, keyFunc, Function.identity());
    }

    public static <T,K,V>  Map<K,V> convertMap(Collection<T> from , Function<T,K> keyFunc , Function<T,V> valueFunc) {
        return convertMap(from,keyFunc,valueFunc,(v, v2) -> v,HashMap::new);
    }

    public static <T,K,V> Map<K,V> convertMap(Collection<T> from , Function<T,K> keyFunc , Function<T,V> valFunc , BinaryOperator<V> mergeFunc , Supplier<? extends Map<K,V>> supplier){
        if (isEmpty(from)) {
            return supplier.get();
        }
        return from.stream().collect(Collectors.toMap(keyFunc,valFunc,mergeFunc,supplier));
    }

    public static <T, K> Map<K, List<T>> convertMultiMap(Collection<T> from, Function<T, K> keyFunc) {
        if (CollUtil.isEmpty(from)) {
            return MapUtil.empty();
        }
        return from.stream().collect(Collectors.groupingBy(keyFunc, Collectors.mapping(t -> t, Collectors.toList())));
    }

    public static <T,R> Set<R> convertSet(Collection<T> from , Function<T,R> func){
        if (isEmpty(from)){
            return Sets.newConcurrentHashSet();
        }
        return toSet(from.stream().map(func));
    }

    public static <T> List<T>  filterToList(Collection<T> from , Predicate<T> predicate){
        if (isEmpty(from)){
            return Lists.newArrayList();
        }
        return toList(from.stream().filter(predicate));
    }

    public static <T, U> List<U> convertList(Collection<T> from, Function<T, U> func) {
        if (isEmpty(from)){
            return Lists.newArrayList();
        }
        return toList(from.stream().map(func));
    }

    private static <T> List<T> toList(Stream<T> stream) {
        return stream.collect(Collectors.toList());
    }

    private static <T> Set<T> toSet(Stream<T> stream) {
        return stream.collect(Collectors.toSet());
    }

    public static <T,U>boolean anyMatch(List<T> patterns, U uri , BiPredicate<T,U> biPredicate) {
        if (isEmpty(patterns)) {
            return false;
        }
        return patterns.stream().anyMatch(pattern -> biPredicate.test(pattern,uri));
    }
}
