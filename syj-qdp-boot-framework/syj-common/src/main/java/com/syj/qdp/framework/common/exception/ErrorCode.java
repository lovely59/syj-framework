package com.syj.qdp.framework.common.exception;

import com.syj.qdp.framework.common.exception.enums.GlobalErrorCodeConstants;
import com.syj.qdp.framework.common.exception.enums.ServiceErrorCodeRange;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 错误码区间 {@link ServiceErrorCodeRange}
 * 全局错误码 参见：{@link GlobalErrorCodeConstants}
 * @author Lyon
 */
@Data
@AllArgsConstructor
public class ErrorCode {

    private Integer code;
    private String msg;

}
