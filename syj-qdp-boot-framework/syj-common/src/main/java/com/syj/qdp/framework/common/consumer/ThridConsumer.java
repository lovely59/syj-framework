package com.syj.qdp.framework.common.consumer;

import java.io.IOException;

/**
 * @author Lyon
 */
@FunctionalInterface
public interface ThridConsumer<T, K, V> {

    void accept(T t, K k, V v) throws IOException;

}
