package com.syj.qdp.framework.common.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用排序模型
 * @author Lyon
 */
@Data
public class SortingField implements Serializable {

    /**
     * 升序
     */
    public static final String ORDER_ASC = "ASC";
    /**
     * 降序
     */
    public static final String ORDER_DESC = "DESC";

    /***
     * 排序字段
     */
    private String field;

    /**
     * 排序顺序
     */
    private String order;
}
