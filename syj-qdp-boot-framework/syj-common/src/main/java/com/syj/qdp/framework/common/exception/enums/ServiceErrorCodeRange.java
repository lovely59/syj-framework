package com.syj.qdp.framework.common.exception.enums;

/**
 *
 * 解决各系统模块错误码问题，避免重复，当前接口做规则声明。
 * 一共 10 位，一共分为四段
 * 第一段，1 位，类型
 *          1 - 业务级别异常
 *          x - 预留
 * 第二段，3 位， 系统类型
 *          001 - 用户系统
 *          002 - 商品系统
 *          003 - 订单系统
 * 第三段，3 位， 模块类型
 *          001 - User 模块
 *          002 - Oauth2 模块
 * 第四段， 3 位， 错误码
 *          不限制规则
 *          建议每个模块自增
 * @author Lyon
 */
public interface ServiceErrorCodeRange {

    // 模块错误码声明

    //普通异常-用户系统-用户模块-{*}
    //1 - 001 - 001 - *
}
