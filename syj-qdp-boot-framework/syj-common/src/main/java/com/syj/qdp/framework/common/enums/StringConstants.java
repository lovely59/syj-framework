package com.syj.qdp.framework.common.enums;

/**
 * 字符串相关常量
 *
 * @author Lyon
 */
public interface StringConstants {

    /**
     * {}占位符
     */
    String BUNDLE_PLACE_HOLDER = "{}";

}
