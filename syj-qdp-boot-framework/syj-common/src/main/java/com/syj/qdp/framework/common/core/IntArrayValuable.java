package com.syj.qdp.framework.common.core;

/**
 * @author Lyon
 */
public interface IntArrayValuable {

    /**
     * int[]范围
     * @return
     */
    int[] array();

}
