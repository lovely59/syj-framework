package com.syj.qdp.framework.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 全局用户类型枚举
 * @author Lyon
 */
@AllArgsConstructor
@Getter
public enum UserTypeEnum {

    /**
     * 后端账户类型
     */
    ADMIN(1, "管理员");

    /**
     * 类型
     */
    private final Integer value;
    /**
     * 类型名
     */
    private final String name;

}
