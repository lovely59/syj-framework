package com.syj.qdp.framework.apilog.config;

import com.syj.qdp.framework.apilog.core.filter.ApiAccessLogFilter;
import com.syj.qdp.framework.apilog.core.service.ApiAccessLogFrameworkService;
import com.syj.qdp.framework.common.enums.WebFilterOrderEnum;
import com.syj.qdp.framework.web.config.WebProperties;
import com.syj.qdp.framework.web.util.FilterUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.servlet.ConditionalOnMissingFilterBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * API访问日志自动配置
 *
 * @author Lyon
 */
@Configuration
@EnableConfigurationProperties({ApiLogProperties.class, WebProperties.class})
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class ApiLogAutoConfiguration {

    @Value("spring.application.name")
    String applicationName;

    /**
     * Api 访问日志属性配置
     */
    @Bean
    @ConditionalOnMissingBean
    public ApiLogProperties apiLogProperties() {
        return new ApiLogProperties();
    }

    /**
     * @param accessLogFrameworkService 访问日志处理器
     * @param webProperties      web配置
     */
    @Bean
    @ConditionalOnMissingFilterBean(ApiAccessLogFilter.class)
    @ConditionalOnBean(ApiAccessLogFrameworkService.class)
    public FilterRegistrationBean<ApiAccessLogFilter> apiAccessLogFilter(ApiAccessLogFrameworkService accessLogFrameworkService, WebProperties webProperties, ApiLogProperties apiLogProperties) {
        return FilterUtils.createFilterRegistration(new ApiAccessLogFilter(accessLogFrameworkService, webProperties, apiLogProperties,applicationName), WebFilterOrderEnum.API_ACCESS_LOG_FILTER);
    }
}
