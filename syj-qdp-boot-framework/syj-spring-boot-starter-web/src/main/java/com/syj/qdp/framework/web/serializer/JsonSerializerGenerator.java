package com.syj.qdp.framework.web.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.syj.qdp.framework.common.consumer.ThridConsumer;
import lombok.SneakyThrows;

import java.io.IOException;

/**
 * @author Lyon
 */
public class JsonSerializerGenerator {

    @SneakyThrows
    public static <T> JsonSerializer<T> generateIgnore(Class<T> clazz) {
        return generate(clazz, (t, jsonGenerator, serializerProvider) -> {
            jsonGenerator.writeString("[igonre]");
        });
    }

    public static <T> JsonSerializer<T> generate(Class<T> clazz, ThridConsumer<T, JsonGenerator, SerializerProvider> consumer) {
        return new JsonSerializer<T>() {
            @Override
            public void serialize(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                consumer.accept(t, jsonGenerator, serializerProvider);
            }

            @Override
            public Class<T> handledType() {
                return clazz;
            }
        };
    }


}
