package com.syj.qdp.framework.apilog.core.service;

import com.syj.qdp.framework.apilog.core.service.dto.ApiAccessLogCreateDTO;

import java.util.concurrent.Future;

/**
 * 日志 访问日志 接口
 * @author Lyon
 */
public interface ApiAccessLogFrameworkService {

    /**
     * 异步创建访问日志
     * @param apiAccessLogCreateDTO
     * @return
     */
    Future<Boolean> createApiAccessLogAsync(ApiAccessLogCreateDTO apiAccessLogCreateDTO);

}
