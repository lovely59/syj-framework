package com.syj.qdp.framework.apilog.core.service;

import com.syj.qdp.framework.apilog.core.service.dto.ApiErrorLogCreateDTO;

import java.util.concurrent.Future;

/**
 * API 错误日志 接口
 *
 * @author Lyon
 */
public interface ApiErrorLogFrameworkService {

    /**
     * 异步创建错误日志
     * @param apiErrorLogCreateDTO
     * @return
     */
    Future<Boolean> createApiErrorLogAsync(ApiErrorLogCreateDTO apiErrorLogCreateDTO);


}
