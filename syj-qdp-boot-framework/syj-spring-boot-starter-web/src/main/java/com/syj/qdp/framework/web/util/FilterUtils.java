package com.syj.qdp.framework.web.util;

import org.springframework.boot.web.servlet.FilterRegistrationBean;

import javax.servlet.Filter;

/**
 * Filter 工具包
 * @author Lyon
 */
public class FilterUtils {

    /**
     * 创建过滤器注册Bean实例
     *
     * @param filter 过滤器
     * @param order  顺序
     * @param <T>    过滤器泛型
     * @return
     */
    public static <T extends Filter> FilterRegistrationBean<T> createFilterRegistration(T filter, Integer order) {
        FilterRegistrationBean<T> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.setOrder(order);
        return filterRegistrationBean;
    }

}
