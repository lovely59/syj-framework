package com.syj.qdp.framework.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.List;

/**
 * Xss properties 配置
 * @author Lyon
 */
@Data
@ConfigurationProperties("syj.qdp.xss")
public class XssProperties {

    /**
     * 是否开启Xss过滤器，默认开启
     */
    private boolean enable = true;

    /**
     * Xss 排除过滤
     */
    private List<String> excludes = Collections.emptyList();
}
