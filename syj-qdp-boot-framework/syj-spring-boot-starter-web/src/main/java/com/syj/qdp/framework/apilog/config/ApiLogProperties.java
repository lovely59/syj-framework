package com.syj.qdp.framework.apilog.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 访问日志属性配置
 *
 * @author Lyon
 */
@Data
@Configuration
@ConfigurationProperties("syj.qdp.web.access-log")
public class ApiLogProperties {

    /**
     * 开启API访问日志，默认开启
     */
    private boolean enable = true;

}
