package com.syj.qdp.framework.apilog.core.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 创建访问记录模型
 *
 * @author Lyon
 */
@Data
@Accessors(chain = true)
public class ApiAccessLogCreateDTO implements Serializable {

    /**
     * 链路追踪编号
     */
    private String traceId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 应用名称不能为空
     */
    @NotBlank(message = "应用名不能为空")
    private String applicationName;

    /**
     * 请求路径
     */
    @NotBlank(message = "请求URL不能为空")
    private String requestUrl;

    /**
     * 请求参数
     */
    private String requestParams;

    /***
     * 请求方法名
     */
    @NotBlank(message = "请求方法不能为空")
    private String requestMethod;


    /**
     * 用户IP
     */
    @NotBlank(message = "用户IP不能为空")
    private String userIp;

    /**
     * 用户Agent
     */
    @NotBlank(message = "用户Agent不能为空")
    private String userAgent;

    /**
     * 开始请求时间
     */
    @NotNull(message = "开始请求时间不能为空")
    private Date beginTime;

    /**
     * 结束请求时间
     */
    @NotNull(message = "结束请求时间不能为空")
    private Date endTime;

    /**
     * 访问耗时-毫秒
     */
    @NotNull(message = "执行时长不能为空")
    private Integer duration;

    /**
     * 返回码
     */
    @NotNull(message = "返回码不能为空")
    private Integer resultCode;

    /**
     * 返回结果
     */
    private String resultMsg;

}
