package com.syj.qdp.framework.operatelog.core.aop;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Maps;
import com.syj.qdp.framework.common.pojo.CommonResult;
import com.syj.qdp.framework.common.util.json.JsonUtils;
import com.syj.qdp.framework.common.util.monitor.TracerUtils;
import com.syj.qdp.framework.common.util.servlet.ServletUtils;
import com.syj.qdp.framework.operatelog.core.annotations.OperateLog;
import com.syj.qdp.framework.operatelog.core.dto.OperateLogCreateReqDTO;
import com.syj.qdp.framework.operatelog.core.enums.OperateTypeEnum;
import com.syj.qdp.framework.operatelog.core.service.OperateLogFrameworkService;
import com.syj.qdp.framework.web.util.WebFrameworkUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.Future;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import static com.syj.qdp.framework.common.exception.enums.GlobalErrorCodeConstants.INTERNAL_SERVER_ERROR;
import static com.syj.qdp.framework.common.exception.enums.GlobalErrorCodeConstants.SUCCESS;
import static com.syj.qdp.framework.web.serializer.factory.JsonSerializerFactory.getOrIgnore;

/**
 * @author Lyon
 */
@Aspect
@Slf4j
@RequiredArgsConstructor
public class OperateLogAspect {

    OperateLogFrameworkService operateLogFrameworkService;

    /**
     * 用于记录操作内容的上下文
     *
     * @see OperateLogCreateReqDTO#getContent()
     */
    private static final ThreadLocal<String> CONTENT = new ThreadLocal<>();
    /**
     * 用于记录拓展字段的上下文
     *
     * @see OperateLogCreateReqDTO#getExts()
     */
    private static final ThreadLocal<Map<String, Object>> EXTS = new ThreadLocal<>();

    @Around("@annotation(apiOperation)")
    @SneakyThrows
    public Object around(ProceedingJoinPoint joinPoint, ApiOperation apiOperation) {
        return around(joinPoint, getMethodAnnotation(joinPoint, OperateLog.class), apiOperation);
    }

    /**
     * TODO 测试 !@annotation(apiOperation)
     *
     * @param joinPoint
     * @param operateLog
     * @param apiOperation
     * @return
     */
    @Around("!@annotation(apiOperation) && @annotation(operateLog)")
    public Object around(ProceedingJoinPoint joinPoint, OperateLog operateLog, ApiOperation apiOperation) {
        return around0(joinPoint, operateLog, apiOperation);
    }

    @SneakyThrows
    private Object around0(ProceedingJoinPoint joinPoint, OperateLog operateLog, ApiOperation apiOperation) {
        Object result = null;
        Date startTime = new Date();
        try {
            result = joinPoint.proceed();
            log(joinPoint, operateLog, apiOperation, startTime, result, null);
        } catch (Throwable throwable) {
            log(joinPoint, operateLog, apiOperation, startTime, result, throwable);
            throw throwable;
        } finally {
            clearThreadLocal();
        }
        return result;
    }

    public static void setContent(String content) {
        CONTENT.set(content);
    }

    public static void addExt(String key, Object value) {
        if (EXTS.get() == null) {
            EXTS.set(new HashMap<>());
        }
        EXTS.get().put(key, value);
    }

    private static void clearThreadLocal() {
        CONTENT.remove();
        EXTS.remove();
    }

    private void log(ProceedingJoinPoint joinPoint, OperateLog operateLog, ApiOperation apiOperation, Date startTime, Object result, Throwable exception) {
        try {
            // 判断不记录的情况
            if (!isLogEnable(joinPoint, operateLog)) {
                return;
            }
            // 真正记录操作日志
            this.log0(joinPoint, operateLog, apiOperation, startTime, result, exception);
        } catch (Throwable ex) {
            log.error("[log][记录操作日志时，发生异常，其中参数是 joinPoint({}) operateLog({}) apiOperation({}) result({}) exception({}) ]",
                    joinPoint, operateLog, apiOperation, result, exception, ex);
        }
    }

    private Future<Boolean> log0(ProceedingJoinPoint joinPoint, OperateLog operateLog, ApiOperation apiOperation, Date startTime, Object result, Throwable exception) {
        OperateLogCreateReqDTO operateLogDTO = new OperateLogCreateReqDTO();
        // 补全通用字段
        operateLogDTO.setStartTime(startTime);
        operateLogDTO.setTraceId(TracerUtils.getTraceId());
        // 补充用户信息
        fillUserFields(operateLogDTO);
        // 补全模块信息
        fillModuleFields(operateLogDTO, joinPoint, operateLog, apiOperation);
        // 补全请求信息
        fillRequestFields(operateLogDTO);
        // 补全方法信息
        fillMethodFields(operateLogDTO, joinPoint, operateLog, startTime, result, exception);
        // 异步执行 处理日志
        return operateLogFrameworkService.createOperateLogAsync(operateLogDTO);
    }

    private void fillUserFields(OperateLogCreateReqDTO operateLogDTO) {
        operateLogDTO.setUserId(WebFrameworkUtils.getLoginUserId());
    }

    private void fillModuleFields(OperateLogCreateReqDTO operateLogDTO, ProceedingJoinPoint joinPoint, OperateLog operateLog, ApiOperation apiOperation) {
        // module 属性
        if (operateLog != null && StrUtil.isNotEmpty(operateLog.module())) {
            operateLogDTO.setModule(operateLog.module());
        }
        if (StrUtil.isEmpty(operateLogDTO.getModule())) {
            Api api = getClassAnnotation(joinPoint, Api.class);
            // 优先读取 @API 的 name 属性
            if (api != null) {
                operateLogDTO.setModule(api.value());
            }
            // 读取 @API 的 tags 属性
            if (StrUtil.isEmpty(operateLogDTO.getModule()) && ArrayUtil.isNotEmpty(api.tags())) {
                operateLogDTO.setModule(api.tags()[0]);
            }
        }
        // name 属性
        if (operateLog != null && StrUtil.isNotEmpty(operateLog.name())) {
            operateLogDTO.setName(operateLog.name());
        }
        if (StrUtil.isEmpty(operateLogDTO.getName()) && apiOperation != null) {
            operateLogDTO.setName(apiOperation.value());
        }
        // type 属性
        if (operateLog != null && ArrayUtil.isNotEmpty(operateLog.type())) {
            operateLogDTO.setType(operateLog.type()[0].getType());
        }
        if (operateLogDTO.getType() == null) {
            RequestMethod requestMethod = obtainFirstMatchRequestMethod(obtainRequestMethods(joinPoint));
            OperateTypeEnum operateLogType = convertOperateLogType(requestMethod);
            operateLogDTO.setType(operateLogType != null ? operateLogType.getType() : null);
        }
        // content 和 exts 属性
        operateLogDTO.setContent(CONTENT.get());
        operateLogDTO.setExts(EXTS.get());

    }private static void fillRequestFields(OperateLogCreateReqDTO operateLogDTO) {
        // 获得 Request 对象
        HttpServletRequest request = ServletUtils.getRequest();
        if (request == null) {
            return;
        }
        // 补全请求信息
        operateLogDTO.setRequestMethod(request.getMethod());
        operateLogDTO.setRequestUrl(request.getRequestURI());
        operateLogDTO.setUserIp(ServletUtil.getClientIP(request));
        operateLogDTO.setUserAgent(ServletUtils.getUserAgent(request));
    }

    private static void fillMethodFields(OperateLogCreateReqDTO operateLogDTO,
                                         ProceedingJoinPoint joinPoint, OperateLog operateLog,
                                         Date startTime, Object result, Throwable exception) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        operateLogDTO.setJavaMethod(methodSignature.toString());
        if (operateLog == null || operateLog.logArgs()) {
            operateLogDTO.setJavaMethodArgs(obtainMethodArgs(joinPoint));
        }
        if (operateLog == null || operateLog.logResultData()) {
            operateLogDTO.setResultData(obtainResultData(result));
        }
        operateLogDTO.setDuration((int) (System.currentTimeMillis() - startTime.getTime()));
        // （正常）处理 resultCode 和 resultMsg 字段
        if (result != null) {
            if (result instanceof CommonResult) {
                CommonResult<?> commonResult = (CommonResult<?>) result;
                operateLogDTO.setResultCode(commonResult.getCode());
                operateLogDTO.setResultMsg(commonResult.getMsg());
            } else {
                operateLogDTO.setResultCode(SUCCESS.getCode());
            }
        }
        // （异常）处理 resultCode 和 resultMsg 字段
        if (exception != null) {
            operateLogDTO.setResultCode(INTERNAL_SERVER_ERROR.getCode());
            operateLogDTO.setResultMsg(ExceptionUtil.getRootCauseMessage(exception));
        }
    }

    private static OperateTypeEnum convertOperateLogType(RequestMethod requestMethod) {
        if (requestMethod == null) {
            return null;
        }
        switch (requestMethod) {
            case GET:
                return OperateTypeEnum.GET;
            case POST:
                return OperateTypeEnum.CREATE;
            case PUT:
                return OperateTypeEnum.UPDATE;
            case DELETE:
                return OperateTypeEnum.DELETE;
            default:
                return OperateTypeEnum.OTHER;
        }
    }

    private static RequestMethod obtainFirstMatchRequestMethod(RequestMethod[] requestMethods) {
        if (ArrayUtil.isEmpty(requestMethods)) {
            return null;
        }
        // 优先，匹配最优的 POST、PUT、DELETE
        RequestMethod result = obtainFirstLogRequestMethod(requestMethods);
        if (result != null) {
            return result;
        }
        // 然后，匹配次优的 GET
        result = Arrays.stream(requestMethods).filter(requestMethod -> requestMethod == RequestMethod.GET)
                .findFirst().orElse(null);
        if (result != null) {
            return result;
        }
        // 兜底，获得第一个
        return requestMethods[0];
    }

    private boolean isLogEnable(ProceedingJoinPoint joinPoint, OperateLog operateLog) {
        if (operateLog != null) {
            return operateLog.enable();
        }
        // 只有 @ApiOperation 注解的情况下，只记录 POST、PUT、DELETE 的情况
        return obtainFirstLogRequestMethod(obtainRequestMethods(joinPoint)) != null;
    }

    private static RequestMethod[] obtainRequestMethods(ProceedingJoinPoint joinPoint) {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        RequestMapping requestMapping = AnnotationUtils.findAnnotation(method, RequestMapping.class);
        return requestMapping != null ? requestMapping.method() : new RequestMethod[]{};
    }

    private static RequestMethod obtainFirstLogRequestMethod(RequestMethod[] requestMethods) {
        return Arrays
                .stream(requestMethods)
                .filter(requestMethod ->
                        requestMethod == RequestMethod.POST ||
                                requestMethod == RequestMethod.PUT ||
                                requestMethod == RequestMethod.DELETE
                )
                .findFirst().orElse(null);
    }


    @SuppressWarnings("SameParameterValue")
    private static <T extends Annotation> T getMethodAnnotation(ProceedingJoinPoint joinPoint, Class<T> annotationClass) {
        return ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(annotationClass);
    }

    @SuppressWarnings("SameParameterValue")
    private static <T extends Annotation> T getClassAnnotation(ProceedingJoinPoint joinPoint, Class<T> annotationClass) {
        return ((MethodSignature) joinPoint.getSignature()).getMethod().getDeclaringClass().getAnnotation(annotationClass);
    }

    private static String obtainMethodArgs(ProceedingJoinPoint joinPoint) {
        // TODO 提升：参数脱敏和忽略
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String[] argNames = methodSignature.getParameterNames();
        Object[] argValues = joinPoint.getArgs();
        // 拼接参数
        Map<String, Object> args = Maps.newHashMapWithExpectedSize(argValues.length);
        ObjectMapper httpIgnoreMapper = new ObjectMapper();
        // http 脱敏序列化器
        SimpleModule simpleModule = new SimpleModule("UnSensitiveSerializer", Version.unknownVersion())
                .addSerializer(getOrIgnore(HttpServletRequest.class))
                .addSerializer(getOrIgnore(HttpServletResponse.class))
                .addSerializer(getOrIgnore(MultipartFile.class));
        httpIgnoreMapper.registerModules(simpleModule);
        for (int i = 0; i < argNames.length; i++) {
            String argName = argNames[i];
            Object argValue = argValues[i];
            // 被忽略时，标记为 ignore 字符串，避免和 null 混在一起
//            args.put(argName, !isIgnoreArgs(argValue) ? argValue : "[ignore]");
            args.put(argName,argValue);
        }
        return JsonUtils.toJsonString(args,httpIgnoreMapper);
    }

    private static String obtainResultData(Object result) {
        // TODO 提升：结果脱敏和忽略
        if (result instanceof CommonResult) {
            result = ((CommonResult<?>) result).getData();
        }
        return JsonUtils.toJsonString(result);
    }

    @Deprecated
    private static boolean isIgnoreArgs(Object object) {
        Class<?> clazz = object.getClass();
        // 处理数组的情况
        if (clazz.isArray()) {
            return IntStream.range(0, Array.getLength(object))
                    .anyMatch(index -> isIgnoreArgs(Array.get(object, index)));
        }
        // 递归，处理数组、Collection、Map 的情况
        if (Collection.class.isAssignableFrom(clazz)) {
            return ((Collection<?>) object).stream()
                    .anyMatch((Predicate<Object>) OperateLogAspect::isIgnoreArgs);
        }
        if (Map.class.isAssignableFrom(clazz)) {
            return isIgnoreArgs(((Map<?, ?>) object).values());
        }
        // obj
        return object instanceof MultipartFile
                || object instanceof HttpServletRequest
                || object instanceof HttpServletResponse;
    }

}
