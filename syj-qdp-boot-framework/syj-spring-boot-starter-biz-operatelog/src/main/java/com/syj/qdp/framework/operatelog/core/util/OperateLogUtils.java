package com.syj.qdp.framework.operatelog.core.util;


import com.syj.qdp.framework.operatelog.core.aop.OperateLogAspect;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * 操作日志工具类
 * 目前主要的作用，是提供给业务代码，记录操作明细和拓展字段
 *
 * @author Lyon
 */
public class OperateLogUtils {

    public static void setContent(String content) {
        OperateLogAspect.setContent(content);
    }

    public static void addExt(String key, Object value) {
        OperateLogAspect.addExt(key, value);
    }

    // TODO
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/Users/lyon/Desktop/未命名.txt");
        System.err.println(path.toFile().exists());
        Stream<String> lines = Files.lines(path);
        System.out.println(lines.count());
    }

}
