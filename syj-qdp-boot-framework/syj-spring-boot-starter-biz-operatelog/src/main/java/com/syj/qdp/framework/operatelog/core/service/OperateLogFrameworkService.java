package com.syj.qdp.framework.operatelog.core.service;

import com.syj.qdp.framework.operatelog.core.dto.OperateLogCreateReqDTO;

import java.util.concurrent.Future;

/**
 * 操作日志框架服务
 * @author Lyon
 */
public interface OperateLogFrameworkService {

    /**
     * 异步处理操作日志
     * @param req 操作日志DTO
     * @return
     */
    Future<Boolean> createOperateLogAsync(OperateLogCreateReqDTO req);

}
