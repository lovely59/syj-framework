package com.syj.qdp.framework.dict.config;

import com.syj.qdp.framework.dict.core.service.DictDataFrameworkService;
import com.syj.qdp.framework.dict.core.util.DictFrameworkUtils;
import org.springframework.context.annotation.Bean;

/**
 * @author Lyon
 */
public class DictDataAutoConfiguration {

    @Bean
    public DictFrameworkUtils dictFrameworkUtils(DictDataFrameworkService dictDataFrameworkService) {
        DictFrameworkUtils.init(dictDataFrameworkService);
        return new DictFrameworkUtils();
    }
}
