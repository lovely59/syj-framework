package com.syj.qdp.framework.security.core.service;

import com.syj.qdp.framework.security.LoginUser;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 用户认证相关服务
 * @author Lyon
 */
public interface SecurityAuthFrameworkService extends UserDetailsService {

    /**
     * 认证令牌并刷新
     * @param token token
     * @returns: {@link LoginUser} 返回token
     */
    LoginUser verifyTokenAndRefresh(String token);

    /**
     * 基于 token 退出登录
     *
     * @param token token
     */
    void logout(String token);

}
