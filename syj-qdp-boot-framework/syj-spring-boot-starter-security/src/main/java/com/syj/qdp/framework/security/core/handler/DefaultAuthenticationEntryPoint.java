package com.syj.qdp.framework.security.core.handler;

import com.syj.qdp.framework.common.exception.enums.GlobalErrorCodeConstants;
import com.syj.qdp.framework.common.pojo.CommonResult;
import com.syj.qdp.framework.common.util.servlet.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.syj.qdp.framework.common.pojo.CommonResult.error;

/**
 * 用户未认证时的访问失败处理器
 *
 * @author Lyon
 */
@Slf4j
public class DefaultAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) throws IOException, ServletException {
        log.warn("[commence] 访问URL({})时，用户未进行登录", request.getRequestURI(), ex);
        ServletUtils.writeJSON(response, CommonResult.error(GlobalErrorCodeConstants.UNAUTHORIZED));
    }
}
