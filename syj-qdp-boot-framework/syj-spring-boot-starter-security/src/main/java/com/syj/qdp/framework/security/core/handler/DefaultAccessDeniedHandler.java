package com.syj.qdp.framework.security.core.handler;

import com.syj.qdp.framework.common.exception.enums.GlobalErrorCodeConstants;
import com.syj.qdp.framework.common.pojo.CommonResult;
import com.syj.qdp.framework.common.util.servlet.ServletUtils;
import com.syj.qdp.framework.web.util.WebFrameworkUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.syj.qdp.framework.common.pojo.CommonResult.error;

/**
 * 默认的 用户已经认证 但是没有权限时的失败处理器
 *
 * @author Lyon
 */
@Slf4j
public class DefaultAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) throws IOException, ServletException {
        log.warn("[commence] 访问URL({})时，用户[{}]没有权限 ", request.getRequestURI(), WebFrameworkUtils.getLoginUserId(), ex);
        ServletUtils.writeJSON(response, CommonResult.error(GlobalErrorCodeConstants.FORBIDDEN));
    }
}
