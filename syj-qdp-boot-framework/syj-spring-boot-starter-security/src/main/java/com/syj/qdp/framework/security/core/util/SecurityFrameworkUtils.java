package com.syj.qdp.framework.security.core.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.syj.qdp.framework.security.LoginUser;
import com.syj.qdp.framework.web.util.WebFrameworkUtils;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Set;

/**
 * 安全认证相关工具包
 *
 * @author Lyon
 */
public class SecurityFrameworkUtils {

    /**
     * 获取用户授权token
     *
     * @param request     请求
     * @param tokenHeader token请求头名称
     * @return
     */
    public static String obtainAuthorizationToken(HttpServletRequest request, String tokenHeader) {
        if (!ObjectUtil.isAllNotEmpty(request, tokenHeader)) {
            return null;
        }
        String authorizationToken = request.getHeader(tokenHeader);
        if (StrUtil.isBlank(tokenHeader) || StrUtil.isBlank(authorizationToken)) {
            return null;
        }
        int index = authorizationToken.indexOf("Bearer ");
        if (index == -1) {
            return null;
        }
        return authorizationToken.substring(index + 7).trim();
    }

    /**
     * 从Security上下文获取登录用户
     * @return
     */
    public static LoginUser getLoginUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (Objects.isNull(context)) {
            return null;
        }
        Authentication authentication = context.getAuthentication();
        if (Objects.isNull(authentication)) {
            return null;
        }
        return authentication.getPrincipal() instanceof LoginUser ? (LoginUser) authentication.getPrincipal() : null;
    }

    /**
     * 获得当前用户的编号，从上下文中
     *
     * @return 用户编号
     */
    @Nullable
    public static Long getLoginUserId() {
        LoginUser loginUser = getLoginUser();
        return loginUser != null ? loginUser.getId() : null;
    }

    /**
     * 获得当前用户的角色编号数组
     *
     * @return 角色编号数组
     */
    @Nullable
    public static Set<Long> getLoginUserRoleIds() {
        LoginUser loginUser = getLoginUser();
        return loginUser != null ? loginUser.getRoleIds() : null;
    }

    /**
     * 设置当前用户到Security上下文
     * @param request
     * @param loginUser
     */
    public static void setLoginUser(HttpServletRequest request, LoginUser loginUser) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, null);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        // 设置Secirity 上下文登录状态
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        // 额外设置到 request 中，用于 ApiAccessLogFilter 可以获取到用户编号；
        // 原因是，Spring Security 的 Filter 在 ApiAccessLogFilter 后面，在它记录访问日志时，线上上下文已经没有用户编号等信息
        WebFrameworkUtils.setLoginUserId(request,loginUser.getId());
    }
}
