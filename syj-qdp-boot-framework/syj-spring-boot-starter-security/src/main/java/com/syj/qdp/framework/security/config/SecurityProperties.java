package com.syj.qdp.framework.security.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.util.Collections;
import java.util.List;

/**
 * Web-Secutiry 属性配置
 *
 * @author Lyon
 */
@ConfigurationProperties("syj.qdp.web.security")
@Data
public class SecurityProperties {

    /**
     * 需要忽略所有权限的url列表
     */
    private List<String> ignoreUrls = Collections.emptyList();

    /**
     * HTTP 请求时，访问令牌的请求 Header
     */
    @NotNull(message = "token-header 不能为空")
    private String tokenHeader;

    /**
     * token 密钥
     */
//    @NotNull(message = "token-secret 不能为空")
//    private String tokenSecret;

    /**
     * token 超时时间
     */
    @NotNull(message = "token-timeout 不能为空")
    private Duration tokenTimeout;

    /**
     * Session 会话过期时间
     */
    @NotNull(message = "session-timeout 不能为空")
    private Duration sessionTimeout;

}
